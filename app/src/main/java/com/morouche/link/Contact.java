package com.morouche.link;

import android.net.Uri;

/**
 * Created by Mehdi on 11/04/2017.
 */
public class Contact {

    private String nom, telephone, id, lookUpKey;
    private Uri image;

    public Contact() {

    }

    public void setLookUpKey(String lookUpKey) {
        this.lookUpKey = lookUpKey;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setImage(Uri image) {
        this.image = image;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getLookUpKey() {
        return this.lookUpKey;
    }

    public String getId() {
        return this.id;
    }

    public Uri getImage() {
        return this.image;
    }

    public String getNom() {
        return this.nom;
    }

    public String getTelephone() {
        return this.telephone;
    }
}