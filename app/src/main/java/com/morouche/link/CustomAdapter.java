package com.morouche.link;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mehdi on 11/04/2017.
 */
public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.MyViewHolder> implements Filterable {

    private List<Contact> contactList;
    private List<Contact> filteredList;
    private Context context;

    /**
     * View holder class
     * */
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView image;
        public TextView nom, telephone;
        public ImageButton appel;

        public MyViewHolder(View view) {
            super(view);
            image = (ImageView) view.findViewById(R.id.image);
            nom = (TextView) view.findViewById(R.id.nom);
            telephone = (TextView) view.findViewById(R.id.telephone);
            appel = (ImageButton) view.findViewById(R.id.appel);
        }
    }

    public CustomAdapter(Context context, List<Contact> contactList) {
        this.contactList = contactList;
        this.filteredList = contactList;
        this.context = context;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final Contact contact = contactList.get(position);


        if(contact.getImage() != null)
            holder.image.setImageURI(contact.getImage());
        else
            holder.image.setImageResource(R.drawable.contact);

        holder.nom.setText(contact.getNom());

        holder.telephone.setText(contact.getTelephone());

        holder.appel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:" + contact.getTelephone()));
                context.startActivity(callIntent);
            }
        });

        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) context).showPopup(contact);
            }
        });
    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_items_listview,parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public Filter getFilter() {
        return new Filter() {

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults oReturn = new FilterResults();
                final ArrayList<Contact> results = new ArrayList<Contact>();
                if (filteredList == null)
                    filteredList = contactList;
                if (constraint != null) {
                    if (filteredList != null && filteredList.size() > 0) {
                        for (final Contact c : filteredList) {
                            if (c.getNom().toLowerCase().contains(constraint.toString()))
                                results.add(c);
                        }
                    }
                    oReturn.values = results;
                }
                return oReturn;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                contactList = (ArrayList<Contact>) results.values;
                notifyDataSetChanged();
            }
        };
    }
}