package com.morouche.link;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.SearchView;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;

public class MainActivity extends AppCompatActivity  {

    private RecyclerView listView ;
    private ArrayList<Contact> StoreContacts ;
    private CustomAdapter arrayAdapter ;
    private Cursor cursor ;
    private String nom, telephone, id, lookUpKey;
    private Uri image;
    public  static final int RequestPermissionCode  = 1 ;
    private SearchView sv;
    private static final int REQUEST_CODE_MODIF= 2;
    private static final int REQUEST_CODE_CREATE= 3;
    private FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        listView = (RecyclerView)findViewById(R.id.listview1);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        listView.setLayoutManager(llm);

        StoreContacts = new ArrayList<Contact>();
        arrayAdapter = new CustomAdapter(this, StoreContacts);
        listView.setAdapter(arrayAdapter);

        fab = (FloatingActionButton)findViewById(R.id.fab);

        listView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if(dy > 0){
                    fab.hide();
                } else{
                    fab.show();
                }

                super.onScrolled(recyclerView, dx, dy);
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ContactsContract.Intents.Insert.ACTION);
                intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
                startActivityForResult(intent, REQUEST_CODE_CREATE);
            }
        });

        new Thread(new Runnable() {
            public void run() {
                EnableRuntimePermission();
            }
        }).start();

    }

    public void GetContacts(){

        StoreContacts.clear();
        cursor = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,null, null, null);

        int result;

        while (cursor.moveToNext()) {

            result = 0;

            Contact c1 = new Contact();

            id = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));

            lookUpKey = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.LOOKUP_KEY));

            nom = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));

            telephone = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER));

            if (cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI)) != null) {
                image = Uri.parse(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI)));
                c1.setImage(image);
            }

            c1.setId(id);
            c1.setLookUpKey(lookUpKey);
            c1.setNom(nom);
            c1.setTelephone(telephone);

            if(StoreContacts.size() == 0)
                StoreContacts.add(c1);
            else {
                for(Contact c : StoreContacts) {
                    if(c.getId().equals(id)) {
                        result = 1;
                        break;
                    }
                }

                if(result == 0)
                    StoreContacts.add(c1);
            }
        }
        cursor.close();
    }

    public void EnableRuntimePermission(){
        if (ContextCompat.checkSelfPermission(this,Manifest.permission.WRITE_CONTACTS) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_CONTACTS)) {

            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_CONTACTS}, RequestPermissionCode);
            }
        }

        if (ContextCompat.checkSelfPermission(this,Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_CONTACTS)) {

            } else {

                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_CONTACTS}, RequestPermissionCode);
            }
        }
        else {
            load();
        }
    }

    @Override
    public void onRequestPermissionsResult(int RC, String per[], int[] PResult) {
        switch (RC) {
            case RequestPermissionCode:
                if (PResult.length > 0 && PResult[0] == PackageManager.PERMISSION_GRANTED) {
                    load();
                }
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        final MenuItem item = menu.findItem(R.id.search);
        sv = new SearchView(getSupportActionBar().getThemedContext());
        sv.setMaxWidth(Integer.MAX_VALUE);

        item.setActionView(sv);

        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                arrayAdapter.getFilter().filter(newText);

                return false;
            }
        });

        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_MODIF) {
            load();
        }
        if (requestCode == REQUEST_CODE_CREATE ){
            load();
        }
    }

    public void load() {
        GetContacts();

        Collections.sort(StoreContacts, new Comparator<Contact>() {
            private final Collator collator = Collator.getInstance(Locale.getDefault());

            public int compare(Contact c1, Contact c2) {
                return collator.compare(c1.getNom(), c2.getNom());
            }
        });
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                arrayAdapter.notifyDataSetChanged();
            }
        });
    }

    public void showPopup(final Contact c) {
        CharSequence colors[] = new CharSequence[] {"Modifier le contact", "Supprimer le contact"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Que voulez vous faire?");
        builder.setItems(colors, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(which == 0)
                    modifContact(c);
                else if(which == 1)
                    deleteContact(getApplicationContext(), c);
            }
        });
        builder.show();
    }

    public void modifContact(Contact c) {
        Uri mSelectedContactUri;
        mSelectedContactUri = ContactsContract.Contacts.getLookupUri(Long.parseLong(c.getId()), c.getLookUpKey());
        Intent editIntent = new Intent(Intent.ACTION_EDIT);
        editIntent.setDataAndType(mSelectedContactUri, ContactsContract.Contacts.CONTENT_ITEM_TYPE);
        startActivityForResult(editIntent, REQUEST_CODE_MODIF);
    }

    public void deleteContact(Context ctx, Contact c) {
        Uri contactUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(c.getTelephone()));
        Cursor cur = ctx.getContentResolver().query(contactUri, null, null, null, null);
        try {
            if (cur.moveToFirst()) {
                do {
                    if (cur.getString(cur.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME)).equalsIgnoreCase(c.getNom())) {
                        String lookupKey = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.LOOKUP_KEY));
                        Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_LOOKUP_URI, lookupKey);
                        ctx.getContentResolver().delete(uri, null, null);
                    }

                } while (cur.moveToNext());
            }
            cur.close();

        } catch (Exception e) {
        }
        load();
    }
}